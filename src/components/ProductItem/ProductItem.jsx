import React from "react";
import "./ProductItem.css";
import { useDispatch } from "react-redux";
import { createTodoAsync } from "../../store/todoApp/todoAppActions";

const ProductItem = (props) => {
  const BASE_URL = "https://63bffb63a177ed68abbccdbe.mockapi.io/cart";
  const dispatch = useDispatch();

  const handleCreateTodo = (e) => {
    const item = {
      title: props.title,
      description: props.description,
      price: props.price,
      discount: props.discount,
      image: props.image,
      category: props.category,
    };

    dispatch(
      createTodoAsync({
        url: BASE_URL,
        el: item,
      })
    );
  };

  return (
    <div className="product-item">
      <img src={props.image} alt="thumb_product.png" />
      <h2>{props.title}</h2>
      <p> {props.description} </p>
      <div className="p__infor">
        <h2>{props.price} USD</h2>
        <button onClick={(e) => handleCreateTodo(e)}>Buy now</button>
      </div>
    </div>
  );
};

export default ProductItem;
