import React, { useEffect } from "react";
import CartItem from "../CartItem/CartItem";
import "./ListItemCart.css";
import { useDispatch, useSelector } from "react-redux";
import { fetchListTodo } from "../../store/todoApp/todoAppActions";

const ListItemCart = () => {

  const BASE_URL = "https://63bffb63a177ed68abbccdbe.mockapi.io/cart";
  const dispatch = useDispatch();
  const listCart = useSelector((state) => state.todoApp);

  const fetchTodoList = async () => {
    dispatch(fetchListTodo(BASE_URL));
  };

  useEffect(() => {
    fetchTodoList();
  }, []);


  return (
    <div className="m-list-cart-container">
      <h1>Order Summary</h1>
      <p>
        Price can change depending on shipping method and taxes of your state.
      </p>

      <div className="cart-item-container">
       {listCart.map((item, index) => (
                <CartItem
                title={item.title}
                description={item.description}
                price={item.price}
                discount={item.discount}
                image={item.image}
                category={item.category}
                id={item.id}
                key={index}
              />
            ))}
      </div>

      <div className="cart-billing-container">
        <div className="cart-subtal">
          <div>
            <p>Subtotal</p>
            <p>73.98 USD</p>
          </div>
          <div>
            <p>Tax</p>
            <p>17% 16.53 USD</p>
          </div>

          <div>
            <p>Shipping</p>
            <p>0 USD</p>
          </div>
        </div>
        <div className="cart-apply-code">
          <input type="text" placeholder="Apply promo code" />
          <img src="/icons/Apply _now.svg" alt="Apply _now" />
        </div>

        <div className="cart-total-order">
          <div>
            <h3>Total Order</h3>
            <p>Guaranteed delivery day: June 12, 2020</p>
          </div>
          <h1>89.84 USD</h1>
        </div>
      </div>
    </div>
  );
};

export default ListItemCart;
