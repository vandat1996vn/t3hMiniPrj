import { createSlice } from "@reduxjs/toolkit";
import { deleteTodoAsync, fetchListTodo, createTodoAsync } from "./todoAppActions";

const initialState = [
];

export const todoAppSlice = createSlice({
  name: "todoAppSlice",
  initialState,
  reducers: {
  },

  extraReducers: (builder) => {
    builder.addCase(fetchListTodo.fulfilled, (state, action) => {
      return [...action.payload];
    });
    builder.addCase(deleteTodoAsync.fulfilled, (state, action) => {
      return state.filter((item) => item.id !== action.payload.id);
    });
    builder.addCase(createTodoAsync.fulfilled, (state, action) => {
      return [...state, ...action.payload];
    });
  },
});

export const todoAppReducer = todoAppSlice.reducer;
